FROM openjdk:8-jdk
COPY target/*.jar gitlabci-test.jar
EXPOSE 9090
ENTRYPOINT ["java","-jar","gitlabci-test.jar"]