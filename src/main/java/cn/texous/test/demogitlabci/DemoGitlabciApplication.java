package cn.texous.test.demogitlabci;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoGitlabciApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoGitlabciApplication.class, args);
    }

}
