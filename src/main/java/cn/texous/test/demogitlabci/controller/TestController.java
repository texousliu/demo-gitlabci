package cn.texous.test.demogitlabci.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * insert desciption here
 *
 * @author Showa.L
 * @since 2020/1/7 17:36
 */
@RestController
public class TestController {

    @GetMapping("/test")
    public String test() {
        System.out.println("test");
        return "test";
    }

    @GetMapping("/test1")
    public String test1() {
        System.out.println("test3");
        return "test3";
    }

    @GetMapping("/test2")
    public String test2() {
        System.out.println("test2");
        return "test2";
    }

}
